﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using CommonUtilities;
using CPIPAutomation.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CPIPAutomation.TestCases
{
    [TestFixture]
    public class SmokeTests : BaseTest
    {

        /// <summary>
        /// Logs into each CPIP site as basic installation / upgrade test
        /// </summary>
        /// <param name="url"></param>
        [Test]
        public void QuickLoginCheck(
            [Values("http://cpip-standalone/IRB", "http://cpip-standalone/COI", "http://cpip-standalone/IACUC", "http://cpip-standalone/Grants", 
                "http://cpip-standalone/CTMS", "http://cpip-standalone/Agreements")] string url)
        {
            Store.BaseUrl = url;
            Store.LoginAsUser(Users.Admin);
            Assert.IsTrue(Web.Driver.Title == "Site Administration","Login failed for store:  " + url);
            
            // needs to verify that the actual build is 6.2
            const string cpversion = "6.2.0";
            var versionInformation = new VersionInformation();
            versionInformation.NavigateTo();
            Assert.IsTrue(versionInformation.ValidateClickPortalFrameworkVersion(cpversion), "Framework version is not:  " + cpversion);
        }

        /// <summary>
        /// Logs into each CPIP-QA2 site as basic installation / upgrade test
        /// </summary>
        /// <param name="url"></param>
        [Test]
        public void QuickLoginCheck_CPIPQA2(
            [Values("http://cpip-qa2/IRB", "http://cpip-qa2/COI", "http://cpip-qa2/IACUC", "http://cpip-qa2/Grants",
                "http://cpip-qa2/CTMS", "http://cpip-qa2/Agreements")] string url)
        {
            Store.BaseUrl = url;
            Store.LoginAsUser(Users.Admin);
            Assert.IsTrue(Web.Driver.Title == "Site Administration", "Login failed for store:  " + url);

            // needs to verify that the actual build is 6.2
            const string cpversion = "6.2.0";
            var versionInformation = new VersionInformation();
            versionInformation.NavigateTo();
            Assert.IsTrue(versionInformation.ValidateClickPortalFrameworkVersion(cpversion), "Framework version is not:  " + cpversion);
        }

        /// <summary>
        /// Creates a person on the host profile data store (IRB), checks the other stores and verifies the user exists
        /// </summary>
        [Test]
        [Description("Creates a person on the host profile data store (IRB), checks the other stores and verifies the user exists")]
        public void QuickSmokeTest()
        {
            // Need to separate base url from store
            string webServerUrl = ClickPortalUI.AutoConfig["WebServer"];
            string baseUrl = Regex.Replace(webServerUrl, @"(http://)(.*)(/.*)", "$1$2");

            // start with IRB
            Store.BaseUrl = baseUrl + @"/IRB";
            string uniqueID = DataGen.String(6);
            string userSuffix = DataGen.String(3);
            string userId = "Marco-" + userSuffix;
            string firstName = userId;
            string lastName = "Polo-" + userSuffix;

            Store.LoginAsUser(Users.Admin);
            var cmd = new CommandWindow();

            var script = @"                
                            var person = Person.createEntity();
                            person.ID = '" + uniqueID + @"';
                            person.userId = '" + userId + @"';
                            person.firstName = '" + firstName + @"';
                            person.lastName = '" + lastName + @"';
                             ";
            cmd.Run(script);
            
            var rmConsolePage = new RMConsole();
            Store.Logout();

            // COI
            // log into each site
            Store.BaseUrl = baseUrl + @"/COI";
            Store.LoginAsUser(Users.Admin);
            rmConsolePage.NavigateTo();
            rmConsolePage.FastFind.Search(FastFind.SearchCategory.Contacts, firstName, lastName);
            Assert.IsTrue(rmConsolePage.FastFind.PersonExists(lastName), lastName + " does not exist in COI integrated store.");
            Store.Logout();

            //// Grants
            Store.BaseUrl = baseUrl + @"/Grants";
            Store.LoginAsUser(Users.Admin);
            rmConsolePage.NavigateTo();
            rmConsolePage.FastFind.Search(FastFind.SearchCategory.Contacts, firstName, lastName);
            Assert.IsTrue(rmConsolePage.FastFind.PersonExists(lastName), lastName + " does not exist in Grants integrated store.");
            Store.Logout();

            //// IACUC
            Store.BaseUrl = baseUrl + @"/IACUC";
            Store.LoginAsUser(Users.Admin);
            rmConsolePage.NavigateTo();
            rmConsolePage.FastFind.Search(FastFind.SearchCategory.Contacts, firstName, lastName);
            Assert.IsTrue(rmConsolePage.FastFind.PersonExists(lastName), lastName + " does not exist in IACUC integrated store.");
            Store.Logout();

            // CTMS
            Store.BaseUrl = baseUrl + @"/CTMS";
            Store.LoginAsUser(Users.Admin);
            rmConsolePage.NavigateTo();
            rmConsolePage.FastFind.Search(FastFind.SearchCategory.Contacts, firstName, lastName);
            Assert.IsTrue(rmConsolePage.FastFind.PersonExists(lastName), lastName + " does not exist in CTMS integrated store.");
            Store.Logout();

            // Agreements
            Store.BaseUrl = baseUrl + @"/Agreements";
            Store.LoginAsUser(Users.Admin);
            rmConsolePage.NavigateTo();
            rmConsolePage.FastFind.Search(FastFind.SearchCategory.Contacts, firstName, lastName);
            Assert.IsTrue(rmConsolePage.FastFind.PersonExists(lastName), lastName + " does not exist in Agreements integrated store.");
            Store.Logout();

        }
    }
}
