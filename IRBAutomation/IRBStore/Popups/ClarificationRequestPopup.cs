﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCWebUIAuto.Pages.BasePages;

namespace IRBAutomation.IRBStore.Popups
{
    public class ClarificationRequestPopup : ActivityPopup
    {
        public ClarificationRequestPopup(string projectId, string activityName) : base(projectId, activityName)
        {
        }
    }
}
