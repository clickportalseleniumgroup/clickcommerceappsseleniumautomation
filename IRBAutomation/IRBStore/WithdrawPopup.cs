﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using NUnit.Framework;
using OpenQA.Selenium;

namespace IRBAutomation.IRBStore
{
    public class WithdrawPopup : ActivityPopup
    {
        public WithdrawPopup(string projectId, string activityName) : base(projectId, activityName)
        {
        }

        public TextBox TxtComment = new TextBox(By.CssSelector("textarea[name='_IRBSubmission_Withdraw.notesAsStr']"));
    }
}
